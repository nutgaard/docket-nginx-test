'use strict';

var fs = require('fs');
var path = require('path');

var express = require('express');
var app = express();

// app.use('/client', express.static(path.join(process.cwd(), '/client')));

app.get('/*', function(req, res) {
    res.render('index', { env: env });
});

var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var webpackDevConfig = require('./webpack.config.development');

new WebpackDevServer(webpack(webpackDevConfig), {
    publicPath: '/',
    contentBase: './src/',
    inline: true,
    hot: true,
    stats: false,
    historyApiFallback: true,
    headers: {
        'Access-Control-Allow-Origin': 'http://localhost:3001',
        'Access-Control-Allow-Headers': 'X-Requested-With'
    }
}).listen(3000, 'localhost', function (err) {
    if (err) {
        console.log(err);
    }

    console.log('webpack dev server listening on localhost:3000');
});