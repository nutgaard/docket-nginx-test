var copyfiles = require('copyfiles');
var replace = require("replace");
var glob = require('glob');

const bundlePattern = /bundle.+\.min\.js$/;

function getBundleHash() {
    return new Promise((resolve, reject) => {
        glob('dist/bundle.*.min.js', {}, (er, files) => {
            if (er || files.length === 0) {
                reject();
            } else {
                const match = bundlePattern.exec(files[0]);
                resolve(match[0]);
            }
        });
    });
}

function copyIndex() {
    return new Promise((resolve, reject) => {
        copyfiles(['src/index.html', 'dist'], true, () => {
            console.log('index.html copied');
            resolve();
        });
    })
}

function applyCachebuser(files) {
    replace({
        regex: 'bundle\.js',
        replacement: files,
        paths: ['dist/index.html'],
    })
}

copyIndex()
    .then(getBundleHash)
    .then(applyCachebuser);