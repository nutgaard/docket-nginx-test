import React from "react";
import Dropdown from "./dropdown/dropdown";
import { reduxForm } from "redux-form";
import Sjekkliste from "./filtersystem/sjekkliste";
import history from './history';
import "./home.scss";

function onSubmit({ query }) {
    return (data) => {
        history.push({query: { ...query, ...data }});
    }
}

function Home({ handleSubmit, location, ...props }) {
    console.log('props', props);
    const onSubmitHandler = onSubmit(location);

    return (
        <div className="home">
            <h1>Home</h1>
            <div className="home__filter">
                <Dropdown group="Test" title="Miljø">
                    <Sjekkliste name="test1" handleSubmit={handleSubmit} onSubmit={onSubmitHandler} />
                </Dropdown>
                <Dropdown group="Test" title="Arbeid">
                    <Sjekkliste name="test2" handleSubmit={handleSubmit} onSubmit={onSubmitHandler} />
                </Dropdown>
                <Dropdown group="Test" title="Klassifiering">
                    <Sjekkliste name="test3" handleSubmit={handleSubmit} onSubmit={onSubmitHandler} />
                </Dropdown>
                <Dropdown group="Test" title="Smarthet">
                    <Sjekkliste name="test4" handleSubmit={handleSubmit} onSubmit={onSubmitHandler} />
                </Dropdown>
            </div>
        </div>
    );
}

Home.propTypes = {};

const AugmentedComponent = reduxForm({
    form: 'test',
    initialValues: {
        firstName: false,
        lastName: false,
        email: false
    }
})(Home);

function typeprepper(str) {
    const float = parseFloat(str);
    if (!Number.isNaN(float) && /^\d+(?:\.\d+)?$/.test(str) ) {
        return float;
    } else if (str === 'true') {
        return true;
    } else if (str === 'false') {
        return false;
    } else {
        return str;
    }
}

function HomeWrapper({ location }) {
    const prepped = Object.entries(location.query)
        .map(([key, value]) => {
            return [key, typeprepper(value)];
        })
        .reduce((obj, [key, value]) => ({ ...obj, [key]: value }), {});

    return (
        <AugmentedComponent location={location} initialValues={prepped} />
    )
}

export default {
    c: HomeWrapper
};