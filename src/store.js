import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import { routerReducer, routerMiddleware } from "react-router-redux";
import { browserHistory } from "react-router";
import DevTools from "./devtools";
import reducers from './reducers';

const middleware = routerMiddleware(browserHistory);

const enhancer = compose(
    applyMiddleware(middleware),
    DevTools.instrument()
);

export default createStore(reducers, {}, enhancer);