const TOGGLE_TAB = 'TOGGLE_TAB';

const initalState = {
    // group: tabid
};

export default function reducer(state = initalState, { type, data }) {
    switch (type) {
        case TOGGLE_TAB: {
            const { group, tab} = data;
            if (state[group] && state[group] === tab) {
                return { ...state, [group]: null };
            }
            return { ...state, [group]: tab };
        }
        default: {
            return state;
        }
    }
}

export function toggleTab(group, tab) {
    return { type: TOGGLE_TAB, data: { group, tab }};
}