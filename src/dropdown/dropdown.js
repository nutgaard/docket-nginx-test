import React, { Component, Children, cloneElement } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { guid } from './../utils';
import { toggleTab } from "./dropdown-reducer";
import "./dropdown.scss";

class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.id = guid();
    }
    render() {
        const { title, children, openTab, toggleTab } = this.props;
        const isOpen = this.id === openTab;
        const cls = ['dropdown', isOpen ? 'dropdown--open' : 'dropdown--closed'].join(' ');
        const augmentedChildren = Children.map(children, (child) => cloneElement(child, { closeTab: () => toggleTab(this.props.group, this.id) }));

        return (
            <div className={cls}>
                <button className="dropdown__btn" onClick={() => toggleTab(this.props.group, this.id)}>{title}</button>
                <div className="dropdown__filter">
                    { augmentedChildren }
                </div>
            </div>
        );
    }
}

Dropdown.propTypes = {};

const mapStateToProps = (state, ownProps) => ({
    openTab: state.dropdown[ownProps.group]
});
const mapDispatchToProps = (dispatch) => ({
    ...bindActionCreators({ toggleTab }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Dropdown);
