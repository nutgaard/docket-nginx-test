import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Field, reduxForm } from "redux-form";

function Sjekkliste({ handleSubmit, closeTab, onSubmit }) {
    const submitter = (data) => {
        onSubmit(data);
        closeTab();
    };
    return (
        <form onSubmit={handleSubmit(submitter)} className="sjekkliste">
            <div>
                <label>
                    <Field name="firstName" component="input" type="checkbox"/>
                    First Name
                </label>
            </div>
            <div>
                <label>
                    <Field name="lastName" component="input" type="checkbox"/>
                    Last Name
                </label>
            </div>
            <div>
                <label>
                    <Field name="email" component="input" type="checkbox"/>
                    Email
                </label>
            </div>
            <button type="submit">Close</button>
        </form>
    );
}

Sjekkliste.propTypes = {};

export default Sjekkliste;

