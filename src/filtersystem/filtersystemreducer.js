const ADD_FILTER = 'ADD_FILTER';
const REMOVE_FILTER = 'REMOVE_FILTER';

const initalState = {
    // group: { name: value }
};

export default function reducer(state = initalState, { type, data }) {
    switch (type) {
        case ADD_FILTER: {
            const { group, name, value } = data;

            const groupData = state[group] || {};
            groupData[name] = value;

            return { ...state, [group]: groupData };
        }
        case REMOVE_FILTER: {
            const { group, name } = data;
            return { ...state, [group]: tab };
        }
        default: {
            return state;
        }
    }
}

export function addFilter(group, name, value) {
    return { type: ADD_FILTER, data: { group, name, value } };
}

export function removeFilter(group, name) {
    return { type: REMOVE_FILTER, data: { group, name } };
}
