import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from 'redux-form'
import reduceReducers from 'reduce-reducers';
import dropdownReducer from './dropdown/dropdown-reducer';

export default reduceReducers(combineReducers({
    dropdown: dropdownReducer,
    form: formReducer,
}));