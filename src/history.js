import { syncHistoryWithStore } from "react-router-redux";
import { browserHistory } from "react-router";
import store from './store';

export default browserHistory;

// export default syncHistoryWithStore(browserHistory, store);