import React, { Component } from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { Router, Route, browserHistory } from "react-router";
import { syncHistoryWithStore, routerReducer, routerMiddleware } from "react-router-redux";
import store from './store';
import history from './history';
import Home from "./home";
import Devtools from './devtools';

function Application({ children }) {
    return (
        <div>
            {children}
            <Devtools />
        </div>
    );
}

class Routes extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router history={history}>
                    <Route component={Application}>
                        <Route path="/" component={Home.c}/>
                    </Route>
                </Router>
            </Provider>
        );
    }
}

render(<Routes />, document.getElementById('app'));